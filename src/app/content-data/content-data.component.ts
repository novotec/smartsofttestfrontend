import { Component, Input, OnInit } from '@angular/core';
import { TableDetailsResponse } from '../interfaces';

@Component({
  selector: 'app-content-data',
  templateUrl: './content-data.component.html',
  styleUrls: ['./content-data.component.scss']
})
export class ContentDataComponent implements OnInit {

  @Input() tableDetails:TableDetailsResponse[] = [];
  constructor() { }

  ngOnInit(): void {
    console.log("tableDetails", this.tableDetails);
  }

}
