//Table interface
export interface TablesResponse {
  id: Number,
  name: String
}

//Data information interface
export interface TableDetailsResponse{
  columns: [],
  id: Number,
  name: String
}
