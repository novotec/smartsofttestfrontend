//Angular Imports
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

//Interface Imports
import { TableDetailsResponse, TablesResponse } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  public apiUrl = "http://localhost:8080/api";
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  constructor(private service: HttpClient) {
  }

  //Get table list
  getTables(){
    return this.service.get<TablesResponse>(`${this.apiUrl}/configuration/getTables`, this.httpOptions);
  }

  //Get table details info
  getTableDetails(tableId: Number){
    if(tableId){
      return this.service.get<TableDetailsResponse>(`${this.apiUrl}/configuration/getTable/${tableId}`, this.httpOptions);
    }
  }

}
