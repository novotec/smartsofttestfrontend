import { Component } from '@angular/core';
import { TablesResponse } from './interfaces';
import { ConfigurationService } from './services/configuration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SmartsoftTestFrontEnd';
  tables: any;
  tableDetails = [];
  selectedTable: TablesResponse;
  constructor(private config_service: ConfigurationService) {
    this.getDataTables();
  }

  //Method for get tables list
  getDataTables(){
    this.config_service.getTables().subscribe(
      (dataTables: any) => {
          if(dataTables){
            //Get table list and default selected table
            this.tables        = dataTables;
            this.selectedTable = dataTables[0];

            //Getting Table Id
            this.changeSelectedTable(this.selectedTable);
          }
      }
    );
  }

  //Method for change table selected
  changeSelectedTable(event: any){
    if(event){
      let tableId = event.id;
      this.tableDetails = [];
      this.config_service.getTableDetails(tableId).subscribe(
        (tableDetails: any) => {
            if(tableDetails){
              setTimeout(() => {
                this.tableDetails = tableDetails;
              }, 200);
              console.log("Table Details: ", this.tableDetails);
            }
        }
      );
    }
  }
}
