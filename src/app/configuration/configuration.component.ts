import { Component, OnInit } from '@angular/core';
import { TablesResponse } from '../interfaces';
import { ConfigurationService } from '../services/configuration.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  tables: any;
  selectedTable: TablesResponse;
  tableDetails: any;
  constructor(private config_service: ConfigurationService) { }

  ngOnInit(): void {
    this.getDataTables();
  }

  getDataTables(){
    this.config_service.getTables().subscribe(
      (dataTables: any) => {
          this.tables        = dataTables;
          this.selectedTable = this.tables[0];
      }
    );
  }

  setSelectedTable(event: any){
    if(event){
      let tableId = event.id;
      this.tableDetails = this.config_service.getTableDetails(tableId);
      console.log(this.tableDetails);
    }
  }

}
